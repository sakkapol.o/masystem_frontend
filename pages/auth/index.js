import { useState, useEffect } from 'react'
import { Table, Space, PageHeader, Button, Spin, Form, Input, notification, Card, Layout, Col, Row, Avatar } from 'antd'
import Link from 'next/link'
import axios from 'axios'
import { get, isNil, eq } from 'lodash'
import { authState } from '../../atom/state'
import { useRecoilState } from 'recoil'
import jwt_decode from "jwt-decode"
import Cookies from 'js-cookie'
import login from '../../images/login.png'

const FormItem = Form.Item
const Meta = Card.Meta
const { Header, Footer, Sider, Content } = Layout;

export default function Auth() {

    const [auth, setAuth] = useRecoilState(authState);

    const onFinish = async (data) => {
        try {
            const result = await axios.post('http://localhost:8080/api/login', data);
            if (eq(200, get(result, 'status'))) {
                const { token } = result?.data
                setAuth(jwt_decode(token))
                Cookies.set('token', token)
                notification.success({ message: 'เข้าสู่ระบบสำเร็จ' })
                window.location.href = `/`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };


    return <>
        <Layout>
            <Header></Header>
            <Content>
                <Row>
                    <Col span={12} offset={6}>
                        <Card cover={<img alt="example" src={login.src}/>} style={{ width: 480, margin: 'auto' }}>
                            <PageHeader
                                title="เข้าสู่ระบบ"
                            >
                                <Form layout="horizontal" onFinish={onFinish} style={{ height: 230}}>
                                    <FormItem name="id" label="ผู้ใช้" labelCol={{ span: 8 }} >
                                        <Input />
                                    </FormItem>
                                    <FormItem name="Password" label="รหัสผ่าน" labelCol={{ span: 8 }} >
                                        <Input type="password" />
                                    </FormItem>
                                    <FormItem style={{ marginTop: 48 }}>
                                        <Button size="large" type="primary" htmlType="submit" block>
                                            เข้าสู่ระบบ
                                        </Button>
                                    </FormItem>
                                </Form>
                            </PageHeader>
                        </Card>
                    </Col>
                </Row>
            </Content>
        </Layout>
    </>
}