import { Form, Button, PageHeader, Input,InputNumber, notification, Select } from 'antd'
import { post } from 'axios';
import { useState, useEffect } from 'react';
import { get, eq, map, toString, set, filter, toInteger, reject } from 'lodash';
import axios, { put } from 'axios';

import withLayout from '../../hoc/withLayout'
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state'

import moment from 'moment';

const FormItem = Form.Item
const TroubleAdd = () => {

    const [user, setUser] = useRecoilState(authState);
    const [customers, setCustomers] = useState([])
    
    const onFinish = async (data) => {
        data.IssueStatus=3
        try {
            //data.IssueDate = moment().format("YYYY-MM-DD HH:MM:SS")
            data.IssueStatus = 'opened'

            let obj = customers.find(f => f.id == data.CustomerId);
            data.EmployeeId = obj.EmployeeId
            !!data.CustomerId && set(data, 'CustomerId', toString(data.CustomerId))
            const result = await post('http://localhost:8080/api/issue', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/issue/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
        console.log('🚀 ~ file: index.js ~ line 28 ~ onFinish ~ data', data)
    };

    useEffect(async () => {
        const customers = get(await axios('http://localhost:8080/api/customer'), 'data', [])
        if (user?.Case === 4) {
            customers = filter(customers, ['id', user?.customerId])
        }
        setCustomers(customers);
    }, [user]);

    function handleChange (value) {
        let obj = customers.find(f => f.id == value);

        console.log(`EmployeeId => ${obj.EmployeeId}`);
    }

    return <>
        <PageHeader
            onBack={() => { window.location.href = `/issue` }}
            title="แจ้งปัญหา"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem
                    name="CustomerId"
                    label="ลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select onChange={handleChange}>
                        {map(customers, ({ id, CustomerName }) => <Select.Option key={id} value={id}>{CustomerName}</Select.Option>)}
                    </Select>
                </FormItem>
                <FormItem
                    name="IssueDetail"
                    label="รายละเอียด"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input.TextArea rows={4} />
                </FormItem>
                <FormItem
                    name="IssueLevel"
                    label="ความสำคัญ"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <InputNumber min={1} max={3} initialValues={3}/>
                </FormItem>  
                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
              
                    <Button size="large" type="primary" htmlType="submit" block>
                        สร้าง
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}

export default withLayout(TroubleAdd, { title: 'TroubleAdd' })