import { useState, useEffect } from 'react';
import { Form, Button, PageHeader, Input, notification, Select, InputNumber, Layout, Spin, DatePicker } from 'antd'
import axios, { put } from 'axios';
import { get, eq, isNil, map, set, toString, toInteger, contains, filter, result } from 'lodash';

import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';

import moment from 'moment';
import { useRouter } from "next/router";
const dateFormat = 'YYYY-MM-DD';
const issuestatus ='';
const FormItem = Form.Item

const TroubleEdit = () => {
    const { id } = useRouter().query;
    const [data, setData] = useState();
    const [customers, setCustomers] = useState([])
    const [employees, setEmployees] = useState([])
    const [user, setUser] = useRecoilState(authState);
    console.log('🚀 ~ file: edit.js ~ line 22 ~ TroubleEdit ~ user', user)

    const onFinish = async (dataForm) => {
        console.log('🚀 ~ file: edit.js ~ line 18 ~ onFinish ~ dataForm', dataForm)
        try {
            !!dataForm?.CustomerId && set(dataForm, 'CustomerId', toString(dataForm?.CustomerId))
            !!dataForm?.EmployeeId && set(dataForm, 'EmployeeId', toString(dataForm?.EmployeeId))
            console.log('🚀 ~ file: edit.js ~ line 27 ~ onFinish ~ dataForm', dataForm)
            !!dataForm?.FixDate && set(dataForm, 'FixDate', moment(get(dataForm, 'FixDate')).format(dateFormat))
            
            const result = await put(`http://localhost:8080/api/issue/${id}`, dataForm);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/issue/${id}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };
    useEffect(async () => {
        if (isNil(id)) return;
        const result = get(await axios(`http://localhost:8080/api/issue/${id}`), 'data');
        
        issuestatus = result?.IssueStatus;
        
        if (!eq('', get(result, 'FixDate', ''))) {
            set(result, 'FixDate', moment(get(result, 'FixDate', '')));
        }
        
        !!result?.CustomerId && set(result, 'CustomerId', toInteger(result?.CustomerId))
        !!result?.EmployeeId && set(result, 'EmployeeId', toInteger(result?.EmployeeId))
        setData(result);
        
        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
        
        const employees = get(await axios("http://localhost:8080/api/employee"), "data", [])
        setEmployees(filter(employees, ({ EmployeePostion }) => EmployeePostion == 'ฝ่ายดูแลระบบ' || EmployeePostion == 'ฝ่ายประสานงาน'));

    }, [id]);

    const isCustomer = user?.Case === 4 ? { display: 'none' } : {}

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }
    const isOption =                        
            <Select style={{ width: 120 }}>
                <Select.Option value="process">opened</Select.Option>
                <Select.Option value="process">process</Select.Option>
                <Select.Option value="close">close</Select.Option>
            </Select>;
    if (issuestatus =='opened'){
            isOption =
                <Select style={{ width: 120 }}>
                    <Select.Option value="process">process</Select.Option>
                    <Select.Option value="close">close</Select.Option>
                </Select>
    } else if (issuestatus =='process'){
        isOption =
        <Select style={{ width: 120 }}>
            <Select.Option value="close">close</Select.Option>
        </Select>
    }

    return (
        <Layout.Content style={{ padding: '0 50px', background: 'white' }}>
            <PageHeader
                onBack={() => { window.location.href = `/issue` }}
                title="แก้ไขการแจ้งปัญหา"
            >
                <Form layout="horizontal" onFinish={onFinish} initialValues={data}>
                    <FormItem
                        name="CustomerId"
                        label="ลูกค้า"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                    >
                        <Select> 
                            {map(customers, ({ id, CustomerName }) => <Select.Option key={id} value={id}>{CustomerName}</Select.Option>)}
                        </Select>
                    </FormItem>
                    <FormItem
                        name="IssueDetail"
                        label="รายละเอียด"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                    >
                        <Input.TextArea rows={4} />
                    </FormItem>
                    
                    <hr/>
                    <FormItem
                        name="EmployeeId"
                        label="พนักงาน"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                        style={isCustomer}
                    >
                        <Select>
                            {map(employees, ({ id, EmployeeName }) => <Select.Option key={id} value={id}>{EmployeeName}</Select.Option>)}
                        </Select>
                    </FormItem>
                    <FormItem
                        name="FixDate"
                        label="วันที่แก้ไข"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                        style={isCustomer}
                    >
                        <DatePicker format={dateFormat} />
                    </FormItem>
                    <FormItem
                        name="FixDetail"
                        label="รายละเอียดที่แก้ไข"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                        style={isCustomer}
                    >
                        <Input.TextArea rows={4} />
                    </FormItem>
                    <FormItem
                        name="IssueLevel"
                        label="ความสำคัญ"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                        style={isCustomer}
                    >
                        <InputNumber max={3} min={1} />
                    </FormItem>
                    <FormItem
                        name="IssueStatus"
                        label="สถานะ"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 8 }}
                        style={isCustomer}
                    >                 
                        {isOption}
                    </FormItem> 

                    <FormItem
                        style={{ marginTop: 48 }}
                        wrapperCol={{ span: 8, offset: 8 }}
                    >
                        <Button size="large" type="primary" htmlType="submit" block>
                            แก้ไข
                        </Button>
                    </FormItem>
                </Form>
            </PageHeader>
        </Layout.Content>
    )
}

export default withLayout(TroubleEdit, { title: 'TroubleEdit' })