import { useState, useEffect } from "react";
import { Descriptions, Tag, Space, PageHeader, Button, Layout, Spin } from "antd";
import Link from "next/link";
import { useRouter } from "next/router";
import axios from "axios";
import { get, isNil, find, toInteger, includes } from "lodash";

import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';

import moment from "moment";
import "moment/locale/th";
moment.locale("th");

const TroubleView = () => {
    const { id } = useRouter().query;
    const [data, setData] = useState();
    console.log('🚀 ~ file: index.js ~ line 19 ~ TroubleView ~ data', data)
    const [customers, setCustomers] = useState([]);
    const [employees, setEmployees] = useState([])
    const [user, setUser] = useRecoilState(authState);

    useEffect(async () => {
        if (isNil(id)) return;

        setData(get(await axios(`http://localhost:8080/api/issue/${id}`), "data"));
        setCustomers(get(await axios("http://localhost:8080/api/customer"), "data", []));
        setEmployees(get(await axios("http://localhost:8080/api/employee"), "data", []));
    }, [id]);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }
    const isTag = <Tag color='geekblue'>{data?.IssueStatus?.toUpperCase()}</Tag>
    if (data?.IssueStatus =='canceled'){
        isTag = <Tag color='red'>{data?.IssueStatus?.toUpperCase()}</Tag>
    }else if(data?.IssueStatus =='close'){
        isTag = <Tag color='green'>{data?.IssueStatus?.toUpperCase()}</Tag>
    }
    return (
        <Layout.Content style={{ padding: '0 50px', background: 'white' }}>
            <PageHeader
                onBack={() => { window.location.href = `/issue` }}
                title={`รายละเอียดการแจ้งปัญหา รหัส ${data?.id}`}
                extra={[
                    includes([0, 2, 3, 4], user?.Case) && (
                    <Link href={`/issue/${id}/edit`}>
                        <a>แก้ไข</a>
                    </Link>
                    )
                ]}
            >
                <Descriptions bordered size="small">
                    <Descriptions.Item label="รหัสปัญหา">{data?.id}</Descriptions.Item>
                    <Descriptions.Item label="รายละเอียดปัญหา" span="2">
                        {data?.IssueDetail}
                    </Descriptions.Item>
                    <Descriptions.Item label="วันที่แจ้งปัญหา">
                        {!!data?.IssueDate && moment(data?.IssueDate).format("lll")}
                    </Descriptions.Item>
                    <Descriptions.Item label="ชื่อลูกค้า" span="2">
                        {find(customers, ["id", toInteger(data?.CustomerId)])
                        ?.CustomerName || data?.CustomerId}
                    </Descriptions.Item>
                    <Descriptions.Item label="วันที่แก้ไข">
                        {!!data?.FixDate && moment(data?.FixDate).format("ll")}
                    </Descriptions.Item>
                    <Descriptions.Item label={"ชื่อพนักงาน"} span="2">
                        {find(employees, ['id', toInteger(data?.EmployeeId)])?.EmployeeName || data?.EmployeeId}
                    </Descriptions.Item>
                    <Descriptions.Item label="สถานะ">
                        {isTag}
                    </Descriptions.Item>
                    <Descriptions.Item label={"ความสำคัญ"} span="2">
                        {data?.IssueLevel}
                    </Descriptions.Item>
                    <Descriptions.Item label="รายละเอียดการแก้ไข" span="3">
                        {data?.FixDetail}
                    </Descriptions.Item>
                </Descriptions>
            </PageHeader>
        </Layout.Content>
    );
}

export default withLayout(TroubleView, { title: 'TroubleView' })