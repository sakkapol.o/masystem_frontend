import { useState, useEffect, useRef } from 'react';
import { Table, Tag, Space, PageHeader, Button, Spin, Typography,notification, message, Popconfirm, Input } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil, find, toInteger, orderBy, includes, filter, reject, eq } from 'lodash'
import { SearchOutlined } from '@ant-design/icons';

import withLayout from '../../hoc/withLayout'
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state'
import Highlighter from 'react-highlight-words';
import { useRouter } from 'next/router'

import moment from 'moment'
import 'moment/locale/th' 
moment.locale('th')


const TroubleList = () => {

    const [filteredInfo, setFilteredInfo] = useState({});
    const [data, setData] = useState()
    const [customers, setCustomers] = useState([])
    const searchInput = useRef(null);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');

    const [user, setUser] = useRecoilState(authState);

    const handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        setFilteredInfo(filters);
        // setSortedInfo(sorter);
    };
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    useEffect(async () => {
        if (!user) return;
        const result = await axios('http://localhost:8080/api/issue');
        let issues = orderBy(get(result, 'data', []), ['IssueDate', 'IssueStatus', 'IssueLevel', 'id'], ['desc', 'desc', 'desc', 'desc'])
        if (user?.Case === 4) {
            issues = filter(issues, ({ CustomerId }) => eq(toInteger(CustomerId), toInteger(get(user, 'customerId', -1))))
        }
        if (user?.Case === 3) {
            issues = filter(issues, ({ EmployeeId }) => eq(toInteger(EmployeeId), toInteger(get(user, 'employeeId', -1))))
        }
        console.log('🚀 ~ file: index.js ~ line 28 ~ useEffect ~ issues', issues)
        setData(issues);
    }, [user]);

    useEffect(async () => {
        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, [])


    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div
                style={{
                padding: 8,
                }}
            >
                <Input
                    ref={searchInput}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{
                        marginBottom: 8,
                        display: 'block',
                    }}
                />
                <Space>
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    icon={<SearchOutlined />}
                    size="small"
                    style={{
                    width: 90,
                    }}
                >
                    Search
                </Button>
                <Button
                    onClick={() => clearFilters && handleReset(clearFilters)}
                    size="small"
                    style={{
                    width: 90,
                    }}
                >
                    Reset
                </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{
                color: filtered ? '#1890ff' : undefined,
                }}
            />
        ),
        onFilter: (value, record) =>{
            return record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        },
        onFilterDropdownVisibleChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.current?.select(), 100);
            }
        },
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{
                        backgroundColor: '#ffc069',
                        padding: 0,
                    }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });



    const columns = [
        // {
        //     title: 'รหัสปัญหา',
        //     dataIndex: 'id',
        // },
        {
            title: 'ลูกค้า',
            width: '12%',
            dataIndex: 'CustomerId',
            key: 'CustomerId',
            render: CustomerId => <p>{find(customers, ['id', toInteger(CustomerId)])?.CustomerName || CustomerId}</p>,
            // filters: customers.map(({ id, CustomerName }) => ({ text: CustomerName, value: id })),
            // filteredValue: filteredInfo.CustomerName || null,
            // onFilter: (value, record) => record?.CustomerName === toString(value),
            // ellipsis: true,
        },
        {
            title: 'หัวข้อ',
            dataIndex: 'IssueDetail',
            key: 'IssueDetail',
            render: text => <Typography.Text style={{ width: 400 }} ellipsis={true}>{text}</Typography.Text>,
        },
        {
            title: 'วันที่แจ้ง',
            width: '10%',
            dataIndex: 'IssueDate',
            render: (d) => moment(d).format('lll'),
        },
        {
            title: 'ความสำคัญ',
            width: '11%',
            align: 'center',
            dataIndex: 'IssueLevel',
            filters: [
                { text: '1', value: 1 },
                { text: '2', value: 2 },
                { text: '3', value: 3 },
            ],
            filteredValue: filteredInfo.IssueLevel || null,
            onFilter: (value, record) => record?.IssueLevel === value,
            sorter: (a, b) => a.IssueLevel - b.IssueLevel,
            ellipsis: true,
        },
        {
            title: 'สถานะ',
            width: '10%',
            dataIndex: 'IssueStatus',
            filters: [
                { text: 'OPENED', value: 'opened' },
                { text: 'PROCESS', value: 'process' },
                { text: 'CLOSE', value: 'close' },
                { text: 'CANCELED', value: 'canceled' },
            ],
            filteredValue: filteredInfo.IssueStatus || null,
            onFilter: (value, record) => record?.IssueStatus === value,
            ellipsis: true,
            key: 'IssueStatus',
            render: status => {
                let color = 'geekblue'
                if (status === 'close') {
                    color = 'green'
                } else if (status === 'canceled'){
                    color = 'red'
                }
                return <Tag color={color} key={status}>{status.toUpperCase()}</Tag>
            }
            
        },
        {
            title: '🎰',
            width: '17%',
            key: 'action',
            render: (text, { id, IssueStatus }) => (
                <Space size="middle">
                    <Link href={`/issue/${id}`}>
                        <a>รายละเอียด</a>
                    </Link>
                    {includes([0, 2, 3, 4], user?.Case) && IssueStatus!=='canceled' && (
                        <Link href={`/issue/${id}/edit`}>
                            <a>แก้ไข</a>
                        </Link>
                    )}
                    {includes([0, 4], user?.Case) && IssueStatus!=='close'&& IssueStatus!=='canceled' &&(
                        <Popconfirm
                            title="คุณต้องการที่จะยกเลิกใช่ไหม?"
                            onConfirm={async (e) => {
                                router.reload(window.location.pathname=`/issue`)
                                await axios.delete(`http://localhost:8080/api/issue/${id}`);
                                message.success('ยกเลิกสำเร็จ');
                                setData(reject(data, ['id', id]));
                            }}
                            onCancel={(e) => {}}
                            okText="ใช่"
                            cancelText="ไม่"
                        >
                            <a href="#">ยกเลิก</a>
                        </Popconfirm>
                    )}
                </Space>
            ),
        },
    ];


    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

        
    const router  = useRouter()
    const handleClick = (e, path) => {
        if(user?.Case==4){
            if(user?.contract.id ==''){
                notification.error({ message: 'ไม่พบข้อมูลสัญญากรุณาติดต่อผู้ดูแลระบบ' })
                return;  
            }
            const d = new Date();
            const enddate = new Date(user?.contract.EndDate);
            debugger;
            if(enddate<=d){
                notification.error({ message: 'สัญญาของท่านหมดอายุกรุณาติดต่อผู้ดูแลระบบ' })
            return;
            }
        }
        debugger;
        router.push(path);
    }
    return (
        <>
            <PageHeader
                title="รายการปัญหา"
                onBack={() => { window.location.href = `/` }}
                extra={[
                    includes([0, 4], user?.Case) && (
                            <Button key="1" onClick={(e) => handleClick(e, `/issue/add`)}>แจ้งปัญหา</Button>
                    )
                ]}
            >
                <Table columns={columns} dataSource={data} onChange={handleChange} />
            </PageHeader>
        </>
    )
}

export default withLayout(TroubleList, { title: 'TroubleList' })