import 'antd/dist/antd.css'
import '../styles/vars.css'
import '../styles/global.css'

import { RecoilRoot } from 'recoil'
import Cookies from 'js-cookie'
import jwt_decode from "jwt-decode"

export default function MyApp({ Component, pageProps }) {
  return <RecoilRoot><Component {...pageProps} /></RecoilRoot>
}
