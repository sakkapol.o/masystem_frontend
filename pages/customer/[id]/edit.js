import { useState, useEffect } from 'react';
import Link from 'next/link'
import { Form, Button, PageHeader, Input, notification, Select, Spin } from 'antd'
//import { get, eq, map, isNil, toString } from 'lodash';
import { get, eq, isNil, map, set, toString, toInteger, contains, filter } from 'lodash';
import axios, { put } from 'axios';
import { useRouter } from "next/router";

import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';

const FormItem = Form.Item

const CustomerEdit = () => {
    const { id } = useRouter().query;
    const [data, setData] = useState();
    const [users, setUsers] = useState([])
    const [employees, setEmployees] = useState([])

    const onFinish = async (dataForm) => {
        try {
            const result = await put(`http://localhost:8080/api/customer/${id}`, { ...dataForm, EmployeeId: toString(get(dataForm, 'EmployeeId')) });
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'แก้ไขข้อมูลสำเร็จ' })
                window.location.href = `/customer/${id}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };
    

    useEffect(async () => {
        if (isNil(id)) return 
        const result = get(await axios(`http://localhost:8080/api/customer/${id}`), 'data')

        !!result?.EmployeeId && set(result, 'EmployeeId', toInteger(result?.EmployeeId))
        setData(result);


        setUsers(get(await axios('http://localhost:8080/api/user'), 'data', []));

        const employees = get(await axios("http://localhost:8080/api/employee"), "data", [])
        setEmployees(filter(employees, ({ EmployeePostion }) => EmployeePostion == 'ฝ่ายดูแลระบบ'));
    }, [id]);


    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            onBack={() => { window.location.href = `/customer` }}
            title="แก้ไขลูกค้า"
        >
            <Form layout="horizontal" onFinish={onFinish} initialValues={data}>
                <Input type="hidden" defaultValue={data?.id} />
                <FormItem
                    name="CustomerName"
                    label="ชื่อลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input defaultValue={data?.CustomerName}/>
                </FormItem>
                <FormItem
                    name="CustomerAddress"
                    label="ที่อยู่ลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input defaultValue={data?.CustomerAddress}/>
                </FormItem>
                <FormItem
                    name="ContactName"
                    label="ชื่อผู้ติดต่อ"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input defaultValue={data?.ContactName}/>
                </FormItem>
                <FormItem
                    name="Telephone"
                    label="เบอร์โทร"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input defaultValue={data?.Telephone}/>
                </FormItem>
                <FormItem
                    name="EmployeeId"
                    label="พนักงานที่ดูแล"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                        <Select>
                            {map(employees, ({ id, EmployeeName }) => <Select.Option key={id} value={id}>{EmployeeName}</Select.Option>)}
                        </Select>
                </FormItem>
                <FormItem
                    name="UserID"
                    label="ผู้ใช้งาน"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select defaultValue={data?.UserID}>
                        {map(users, ({ id }) => <Select.Option value={toString(id)}>{id}</Select.Option>)}
                    </Select>
                </FormItem>

                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit" block>
                        แก้ไข
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}

export default withLayout(CustomerEdit, { title: 'CustomerEdit' })