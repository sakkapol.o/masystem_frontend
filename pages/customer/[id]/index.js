import { useState, useEffect } from 'react';
import { Descriptions, Tag, Space, PageHeader, Button, Layout, Spin } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from 'axios';
import moment from 'moment';
import { get, isNil, find, toInteger, includes } from 'lodash'

import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';

const CustomerView = () => {
    const { id } = useRouter().query
    const [data, setData] = useState()
    const [employees, setEmployees] = useState([])
    const [user, setUser] = useRecoilState(authState);
    useEffect(async () => {
        if (isNil(id)) return 
        const result = await axios(`http://localhost:8080/api/customer/${id}`);
        setData(get(result, 'data'));
        setEmployees(get(await axios('http://localhost:8080/api/employee'), 'data', []));
    }, [id]);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return (
        <Layout.Content style={{ padding: '0 50px', background: 'white' }}>
            <PageHeader
                onBack={() => { window.location.href = `/customer` }}
                title={`รายละเอียดลูกค้า รหัส ${data?.id}`}
                extra={[
                    includes([0, 2], user?.Case) && (
                        <Link href={`/customer/${id}/edit`}>
                            <a>แก้ไข</a>
                        </Link>
                    )
                ]}
            >
                <Descriptions bordered>
                    <Descriptions.Item label="รหัสลูกค้า">
                        {data?.id}
                    </Descriptions.Item>
                    <Descriptions.Item label="ชื่อลูกค้า">
                        {data?.CustomerName}
                    </Descriptions.Item>
                    <Descriptions.Item label="ที่อยู่ลูกค้า">
                        {data?.CustomerAddress}
                    </Descriptions.Item>
                    <Descriptions.Item label="ชื่อผู้ติดต่อ">
                        {data?.ContactName}
                    </Descriptions.Item>
                    <Descriptions.Item label="พนักงานที่ดูแล">
                        <Space>
                        {find(employees, ['id', toInteger(data?.EmployeeId)])?.EmployeeName || data?.EmployeeId}
                        </Space>
                    </Descriptions.Item>
                    <Descriptions.Item label="เบอร์โทร">
                        {data?.Telephone}
                    </Descriptions.Item>
                    <Descriptions.Item label="ผู้ใช้งาน">
                        {data?.UserID}
                    </Descriptions.Item>
                </Descriptions>
            </PageHeader>
        </Layout.Content>
    )
}

export default withLayout(CustomerView, { title: 'CustomerView' })