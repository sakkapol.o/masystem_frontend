import { useState, useEffect } from 'react';
import Link from 'next/link'
import { Form, Button, PageHeader, Input, notification, Select } from 'antd'
//import { get, eq, map } from 'lodash';
import { get, eq, isNil, map, set, toString, toInteger, contains, filter } from 'lodash';
import axios, { post } from 'axios';

import withLayout from '../../hoc/withLayout'
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state'

const FormItem = Form.Item

const CustomerAdd = () => {

    const onFinish = async (data) => {
        try {
            const result = await post('http://localhost:8080/api/customer', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/customer/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };
    
    const [users, setUsers] = useState([])
    const [employees, setEmployees] = useState([])

    useEffect(async () => {
        const users =get(await axios('http://localhost:8080/api/user'), 'data', [])
        setUsers(filter(users, ({ Case }) => Case == 4));
        //setUsers(get(await axios('http://localhost:8080/api/user'), 'data', []));

        const employees = get(await axios("http://localhost:8080/api/employee"), "data", [])
        setEmployees(filter(employees, ({ EmployeePostion }) => EmployeePostion == 'ฝ่ายดูแลระบบ'));
    }, []);

    return <>
        <PageHeader
            onBack={() => { window.location.href = `/customer` }}
            title="สร้างลูกค้า"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem
                    name="CustomerName"
                    label="ชื่อลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="CustomerAddress"
                    label="ที่อยู่ลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="ContactName"
                    label="ชื่อผู้ติดต่อ"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="Telephone"
                    label="เบอร์โทร"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Input />
                </FormItem>
                <FormItem
                    name="EmployeeId"
                    label="พนักงานที่ดูแล"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select>
                        {map(employees, ({ id, EmployeeName }) => <Select.Option key={id}>{EmployeeName}</Select.Option>)}
                    </Select>
                </FormItem>
                <FormItem
                    name="UserID"
                    label="ผู้ใช้งาน"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select>
                        {map(users, ({ id }) => <Select.Option key={id}>{id}</Select.Option>)}
                    </Select>
                </FormItem>

                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit" block>
                        ยืนยัน
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}

export default withLayout(CustomerAdd, { title: 'CustomerAdd' })