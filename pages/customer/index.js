import { useState, useEffect } from 'react';
import { Table, Tag, Space, PageHeader, Button, Spin, Popconfirm, message } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil, orderBy, includes, filter, reject } from 'lodash'

import withLayout from '../../hoc/withLayout'
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state'

const CustomerList = () => {
    
    const [data, setData] = useState()
    const [user, setUser] = useRecoilState(authState);

    useEffect(async () => {
        setData(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, []);


    const columns = [
        {
            title: 'รหัส',
            dataIndex: 'id',
        },
        {
            title: 'ชื่อลูกค้า',
            dataIndex: 'CustomerName',
            key: 'CustomerName',
            render: text => <p>{text}</p>,
        },
        {
            title: 'ชื่อผู้ติดต่อ',
            dataIndex: 'ContactName',
            key: 'ContactName',
            render: text => <p>{text}</p>,
        },
        {
            title: 'เบอรต์ดิดต่อ',
            dataIndex: 'Telephone',
            key: 'Telephone',
            render: text => <p>{text}</p>,
        },
        {
            title: '🎰',
            key: 'action',
            render: (text, { id }) => (
                <Space size="middle">
                    <Link href={`/customer/${id}`}>
                        <a>รายละเอียด</a>
                    </Link>
                    {includes([0, 2], user?.Case) && (
                        <Link href={`/customer/${id}/edit`}>
                            <a>แก้ไข</a>
                        </Link>
                    )}
                    {includes([0], user?.Case) && (
                        <Popconfirm
                            title="คุณต้องการที่จะลบใช่ไหม?"
                            onConfirm={async (e) => {
                                await axios.delete(`http://localhost:8080/api/customer/${id}`);
                                message.success('ลบสำเร็จ');
                                setData(reject(data, ['id', id]));
                            }}
                            onCancel={(e) => {}}
                            okText="ใช่"
                            cancelText="ไม่"
                        >
                            <a href="#">ลบ</a>
                        </Popconfirm>
                    )}
                </Space>
            ),
        },
    ];

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            title="ข้อมูลลูกค้า"
            onBack={() => { window.location.href = `/` }}
            extra={[
                includes([0, 2], user?.Case) && (
                    <Link href={`/customer/add`}>
                        <Button key="1">เพิ่มข้อมูลลูกค้า</Button>
                    </Link>
                )
            ]}
        >
            <Table columns={columns} dataSource={data} />
        </PageHeader>
    </>
}

export default withLayout(CustomerList, { title: 'CustomerList' })