import { Button, Layout, Col, Row, Tag, Card } from 'antd'
import { SmileFilled } from '@ant-design/icons'
import Link from 'next/link'
import jwt_decode from "jwt-decode"
import Cookies from 'js-cookie'
import { useState, useEffect } from 'react'
import includes from 'lodash/includes'
import pic_issue from '../images/issue.png'
import pic_contract from '../images/contract.png'
import pic_customer from '../images/customer.png'
import pic_user from '../images/user.png'

const { Header, Footer, Sider, Content } = Layout;

export default function Home() {

    const [user, setUser] = useState()

    useEffect(() => {
        try {
            setUser(jwt_decode(Cookies.get('token')))
        } catch (error) {
            window.location.href = `/auth`;
        }
    }, [])

    const signOut = () => {
        Cookies.remove('token');
        window.location.href = `/auth`;
    }

    const ROLE = [
        'แอดมิน',
        'ฝ่ายบริหาร',
        'ฝ่ายประสานงาน',
        'ฝ่ายดูแลระบบ',
        'ลูกค้า'
    ]
  
  return (
    <Layout>
        <Header style={{ color: 'white', textAlign: 'right' }}>
            <Tag color="#2db7f5" className="ml-1">{user?.userId} </Tag>
            <Tag color="cyan" className="ml-1">{ROLE[user?.Case]} </Tag>
            <Button type="primary" danger onClick={signOut} className="ml-2" size="small">
                ออกจากระบบ
            </Button>
        </Header>
        <Content>
            <Row>
                <Col span={12} offset={6}>
                    <div className="text-center mb-5 mt-5">
                        {/* <Link href="#">
                          <a className="logo mr-0">
                            <SmileFilled size={48} strokeWidth={1} />
                          </a>
                        </Link> */}

                        <p className="mb-10 mt-10 text-disabled"><font size="36" color ="#616161" >ยินดีต้องรับเข้าสู่ระบบ !</font></p>
                        <p className="mb-10 mt-10 text-disabled"></p>

                        <Row gutter={16} >
                            <Col span={6}>
                                {includes([0, 1, 2, 3, 4], user?.Case)
                                    && (
                                    <Link href="/issue">
                                        <Card
                                            hoverable
                                            style={{ width: 240 }}
                                            cover={<img alt="example" src={pic_issue.src} />}
                                        >
                                            <Card.Meta title="รายการปัญหา" />
                                        </Card>
                                    </Link>
                                )}
                            </Col>
                            <Col span={6}>
                                {includes([0, 1, 2, 3, 4], user?.Case)
                                    && (
                                    <Link href="/contract">
                                        <Card
                                            hoverable
                                            style={{ width: 240 }}
                                            cover={<img alt="example" src={pic_contract.src} />}
                                        >
                                            <Card.Meta title="รายการสัญญา" />
                                        </Card>
                                    </Link>
                                )}
                            </Col>
                            <Col span={6}>
                                {includes([0, 2, 3], user?.Case)
                                    && (
                                    <Link href="/customer">
                                        <Card
                                            hoverable
                                            style={{ width: 240 }}
                                            cover={<img alt="example" src={pic_customer.src} />}
                                        >
                                            <Card.Meta title="ข้อมูลลูกค้า" />
                                        </Card>
                                    </Link>
                                )}
                            </Col>
                            <Col span={6}>
                                {includes([0], user?.Case)
                                    && (
                                    <Link href="/user">
                                        <Card
                                            hoverable
                                            style={{ width: 240 }}
                                            cover={<img alt="example" src={pic_user.src} />}
                                        >
                                            <Card.Meta title="ข้อมูลผู้ใช้งานระบบ" />
                                        </Card>
                                    </Link>
                                )}
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        </Content>
    </Layout>
  )
}
