import { useState, useEffect } from 'react';
import { Table, Space, PageHeader, Button, Spin, Popconfirm, message } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil, orderBy, includes, filter, reject } from 'lodash'

import withLayout from '../../hoc/withLayout'
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state'

import { FORMAT_CASE } from './[id]'

const UserList = () => {

    const [data, setData] = useState()
    const [user, setUser] = useRecoilState(authState);

    useEffect(async () => {
        const result = await axios('http://localhost:8080/api/user');
        setData(get(result, 'data', []));
    }, []);

    const columns = [
        {
            title: 'ผู้ใช้งาน',
            dataIndex: 'id',
        },
        {
            title: 'สิทธิ์การใช้งาน',
            dataIndex: 'Case',
            key: 'Case',
            render: text => <p>{FORMAT_CASE[text]}</p>,
        },
        {
            title: '🎰',
            key: 'action',
            render: (text, { id }) => (
                <Space size="middle">
                    <Link href={`/user/${id}`}>
                        <a>รายละเอียด</a>
                    </Link>
                    <Link href={`/user/${id}/edit`}>
                        <a>แก้ไข</a>
                    </Link>
                    <Popconfirm
                        title="คุณต้องการที่จะลบใช่ไหม?"
                        onConfirm={async (e) => {
                            await axios.delete(`http://localhost:8080/api/user/${id}`);
                            message.success('ลบสำเร็จ');
                            setData(reject(data, ['id', id]));
                        }}
                        onCancel={(e) => {}}
                        okText="ใช่"
                        cancelText="ไม่"
                    >
                        <a href="#">ลบ</a>
                    </Popconfirm>
                </Space>
            ),
        },
    ];

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            title="ข้อมูลผู้ใช้งานระบบ"
            onBack={() => { window.location.href = `/` }}
            extra={[
                includes([0], user?.Case) && (
                    <Link href={`/user/add`}>
                        <Button key="1">เพิ่มผู้ใช้งาน</Button>
                    </Link>
                )
            ]}
        >
            <Table columns={columns} dataSource={data} />
        </PageHeader>
    </>
}

export default withLayout(UserList, { title: 'UserList' })