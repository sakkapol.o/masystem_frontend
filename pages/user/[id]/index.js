import { useState, useEffect } from 'react';
import { Descriptions, PageHeader, Layout, Spin } from 'antd';
import { useRouter } from 'next/router';
import axios from 'axios';
import { get, isNil } from 'lodash'

import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';

export const FORMAT_CASE = ['แอดมิน', 'ฝ่ายบริหาร', 'ฝ่ายประสานงาน', 'ฝ่ายบำรุงรักษา', 'ลูกค้า']

const UserView = () => {
    const { id } = useRouter().query
    const [data, setData] = useState()

    useEffect(async () => {
        if (isNil(id)) return 
        const result = await axios(`http://localhost:8080/api/user/${id}`);
        setData(get(result, 'data'));
    }, [id]);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return (
        <Layout.Content style={{ padding: '0 50px', background: 'white' }}>
            <PageHeader 
                onBack={() => { window.location.href = `/user` }}
                title="รายละเอียด">
                <Descriptions bordered size="small">
                    <Descriptions.Item label="ผู้ใช้งาน">
                        {data?.id}
                    </Descriptions.Item>
                    <Descriptions.Item label="สิทธิ์การใช้งาน">
                        {!!data && FORMAT_CASE[data?.Case]}
                    </Descriptions.Item>
                </Descriptions>
            </PageHeader>
        </Layout.Content>
    )
}
export default withLayout(UserView, { title: 'UserView' })