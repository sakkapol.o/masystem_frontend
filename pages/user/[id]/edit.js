import { Form, Select, Button, PageHeader, Input, notification, Spin } from 'antd'
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import axios, { put } from 'axios';
import { get, eq, isNil, set, toInteger } from 'lodash'

import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';

const FormItem = Form.Item

const UserEdit = () => {
    const { id } = useRouter().query
    const [data, setData] = useState()

    const onFinish = async (dataForm) => {
        try {
            if (isNil(get(dataForm, 'Password')) || eq('', get(dataForm, 'Password'))) {
                return notification.error({ message: 'เกิดข้อผิดพลาด', description: 'กรุณาระบุรหัสผ่านอีกครั้ง' })
            }
            console.log('🚀 ~ file: edit.js ~ line 23 ~ useEffect ~ issues', dataForm);
            const result = await put(`http://localhost:8080/api/user/${id}`, dataForm);
            
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/user/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };

    useEffect(async () => {
        if (isNil(id)) return 
        const result = get(await axios(`http://localhost:8080/api/user/${id}`), 'data')
        set(result, 'Password', '')
        setData(result);
    }, [id]);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            className="site-page-header"
            onBack={() => { window.location.href = `/user` }}
            title="แก้ไขผู้ใช้งาน"
        >
            <Form layout="horizontal" onFinish={onFinish} initialValues={data}>
                <FormItem name="id" label="ผู้ใช้" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Input disabled={true} />
                </FormItem>
                <FormItem name="Password" label="รหัสผ่าน" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Input type="password" />
                </FormItem>
                <FormItem name="Case" label="สิทธิ์การใช้งาน" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Select style={{ width: '100%' }}>
                        <Select.Option value={0}>แอดมิน</Select.Option>
                        <Select.Option value={1}>ฝ่ายบริหาร</Select.Option>
                        <Select.Option value={2}>ฝ่ายประสานงาน</Select.Option>
                        <Select.Option value={3}>ฝ่ายดูแลระบบ</Select.Option>
                        <Select.Option value={4}>ลูกค้า</Select.Option>
                    </Select>
                </FormItem>
                <FormItem style={{ marginTop: 48 }} wrapperCol={{ span: 8, offset: 8 }}>
                    <Button size="large" type="primary" htmlType="submit" block>
                        แก้ไข
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}

export default withLayout(UserEdit, { title: 'UserEdit' })