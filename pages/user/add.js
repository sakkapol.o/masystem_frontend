import { Form, Select, Button, PageHeader, Input, notification } from 'antd'

// Custom DatePicker that uses Day.js instead of Moment.js
import DatePicker from '../../components/DatePicker'
import { post } from 'axios';
import { get, eq } from 'lodash'

import withLayout from '../../hoc/withLayout'
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state'

const FormItem = Form.Item

const UserCreateForm = () => {

    const onFinish = async (data) => {
        try {
            const result = await post('http://localhost:8080/api/user', data);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/user/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };


    return <>
        <PageHeader
            onBack={() => { window.location.href = `/user` }}
            title="สร้างผู้ใช้งาน"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem name="id" label="ผู้ใช้" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Input />
                </FormItem>
                <FormItem name="Password" label="รหัสผ่าน" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Input type="password" />
                </FormItem>
                <FormItem name="Case" label="สิทธิ์การใช้งาน" labelCol={{ span: 8 }} wrapperCol={{ span: 8 }}>
                    <Select defaultValue={0} style={{ width: '100%' }}>
                        <Select.Option value={0}>แอดมิน</Select.Option>
                        <Select.Option value={1}>ฝ่ายบริหาร</Select.Option>
                        <Select.Option value={2}>ฝ่ายประสานงาน</Select.Option>
                        <Select.Option value={3}>ฝ่ายดูแลระบบ</Select.Option>
                        <Select.Option value={4}>ลูกค้า</Select.Option>
                    </Select>
                </FormItem>
                <FormItem style={{ marginTop: 48 }} wrapperCol={{ span: 8, offset: 8 }}>
                    <Button size="large" type="primary" htmlType="submit" block>
                        สร้าง
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}

export default withLayout(UserCreateForm, { title: 'UserCreateForm' })