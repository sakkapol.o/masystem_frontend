import { useState, useEffect } from 'react';
import Link from 'next/link'
import { Form, Button, PageHeader, Input, notification, Select } from 'antd'
import axios, { post } from 'axios';
import { get, eq, map, set, isNil } from 'lodash';

import DatePicker from '../../components/DatePicker'

import withLayout from '../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state';

import moment from 'moment';
import 'moment/locale/th' 
moment.locale('th')
const dateFormat = 'YYYY-MM-DD';

const FormItem = Form.Item

const TroubleList = () => {

    const onFinish = async (dataForm) => {
        try {
            if (isNil(get(dataForm, 'StartDate'))) {
                return notification.error({ message: 'เกิดข้อผิดพลาด', description: 'กรุณาระบุวันเริ่มสัญญา' })
            } else {
                set(dataForm, 'StartDate', get(dataForm, 'StartDate')?.format(dateFormat))
            }
            if (isNil(get(dataForm, 'EndDate'))) {
                return notification.error({ message: 'เกิดข้อผิดพลาด', description: 'กรุณาระบุวันสิ้นสุดสัญญา' })
            } else {
                set(dataForm, 'EndDate', get(dataForm, 'EndDate')?.format(dateFormat))
            }
            const result = await post('http://localhost:8080/api/contract', dataForm);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/contract/${get(result, 'data.data.id')}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };


    const [customers, setCustomers] = useState([])

    useEffect(async () => {
        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, []);

    return <>
        <PageHeader
            onBack={() => { window.location.href = `/contract` }}
            title="เพิ่มสัญญา"
        >
            <Form layout="horizontal" onFinish={onFinish}>
                <FormItem
                    name="CustomerId"
                    label="ลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select>
                        {map(customers, ({ id, CustomerName }) => <Select.Option key={id}>{CustomerName}</Select.Option>)}
                    </Select>
                </FormItem>
                <FormItem
                    name="StartDate"
                    label="วันเริ่มสัญญา"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <DatePicker format={dateFormat} />
                </FormItem>
                <FormItem
                    name="EndDate"
                    label="วันสิ้นสุดสัญญา"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <DatePicker format={dateFormat} />
                </FormItem>

                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit" block>
                        ยืนยัน
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}
export default withLayout(TroubleList, { title: 'TroubleList' })