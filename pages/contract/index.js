import { useState, useEffect } from 'react';
import { Table, Tag, Space, PageHeader, Button, Spin, Popconfirm, message } from 'antd';
import Link from 'next/link';
import axios from 'axios';
import { get, isNil, find, toInteger, includes, orderBy, filter, reject, eq } from 'lodash'

import withLayout from '../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../atom/state';

import moment from 'moment'
import 'moment/locale/th' 
moment.locale('th')

const ContractList = () => {

    const [data, setData] = useState()
    const [customers, setCustomers] = useState([])

    const [user, setUser] = useRecoilState(authState);


    useEffect(async () => {
        if (!user) return;
        const result = await axios('http://localhost:8080/api/contract');
        let contracts = orderBy(get(result, 'data', []), ['IssueDate', 'IssueLevel', 'id'], ['desc', 'desc', 'desc'])
        if (user?.Case === 4) {
            contracts = filter(contracts, ({ CustomerId }) => eq(toInteger(CustomerId), toInteger(get(user, 'customerId', -1))))
        }
        setData(contracts);
    }, [user]);

    useEffect(async () => {
        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, []);


    const columns = [
        {
            title: 'รหัส',
            dataIndex: 'id',
        },
        {
            title: 'ลูกค้า',
            dataIndex: 'CustomerId',
            key: 'CustomerId',
            render: CustomerId => <p>{find(customers, ['id', toInteger(CustomerId)])?.CustomerName || CustomerId}</p>,
        },
        {
            title: 'วันเริ่มสัญญา',
            dataIndex: 'StartDate',
            key: 'StartDate',
            render: text => <p>{moment(text).format('ll')}</p>,
        },
        {
            title: 'วันสิ้นสุดสัญญา',
            dataIndex: 'EndDate',
            key: 'EndDate',
            render: text => <p>{moment(text).format('ll')}</p>,
        },
        {
            title: '🎰',
            key: 'action',
            render: (text, { id }) => (
                <Space size="middle">
                    <Link href={`/contract/${id}`}>
                        <a>รายละเอียด</a>
                    </Link>
                    {includes([0, 2], user?.Case) && (
                        <Link href={`/contract/${id}/edit`}>
                            <a>แก้ไข</a>
                        </Link>
                    )}
                    {includes([0, 2], user?.Case) && (
                        <Popconfirm
                            title="คุณต้องการที่จะลบใช่ไหม?"
                            onConfirm={async (e) => {
                                await axios.delete(`http://localhost:8080/api/contract/${id}`);
                                message.success('ลบสำเร็จ');
                                setData(reject(data, ['id', id]));
                            }}
                            onCancel={(e) => {}}
                            okText="ใช่"
                            cancelText="ไม่"
                        >
                            <a href="#">ลบ</a>
                        </Popconfirm>
                    )}
                </Space>
            ),
        },
    ];

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }
    
    return <>
        <PageHeader
            title="รายการสัญญา"
            onBack={() => { window.location.href = `/` }}
            extra={[
                includes([0, 2], user?.Case) && (
                    <Link href={`/contract/add`}>
                        <Button key="1">เพิ่มสัญญา</Button>
                    </Link>
                )
            ]}
        >
            <Table columns={columns} dataSource={data} />
        </PageHeader>
    </>
}

export default withLayout(ContractList, { title: 'ContractList' })