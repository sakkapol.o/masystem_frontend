import { useState, useEffect } from 'react';
import Link from 'next/link'
import { Form, Button, PageHeader, Input, notification, Select, Spin, DatePicker } from 'antd'
import axios, { put } from 'axios';
import { get, eq, map, set, isNil, toInteger, toString } from 'lodash';
import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';
import moment from 'moment';
import 'moment/locale/th' 
import { useRouter } from 'next/router';
moment.locale('th')
const dateFormat = 'YYYY-MM-DD';

const FormItem = Form.Item

const ConteactEdit = () => {
    const { id } = useRouter().query
    const [data, setData] = useState()
    const [customers, setCustomers] = useState([])

    const onFinish = async (dataForm) => {
        try {
            if (isNil(get(dataForm, 'StartDate'))) {
                return notification.error({ message: 'เกิดข้อผิดพลาด', description: 'กรุณาระบุวันเริ่มสัญญา' })
            } else {
                set(dataForm, 'StartDate', moment(get(dataForm, 'StartDate')).add(7, 'hour').format('YYYY-MM-DD'))
            }
            if (isNil(get(dataForm, 'EndDate'))) {
                return notification.error({ message: 'เกิดข้อผิดพลาด', description: 'กรุณาระบุวันสิ้นสุดสัญญา' })
            } else {
                set(dataForm, 'EndDate', moment(get(dataForm, 'EndDate')).add(7, 'hour').format('YYYY-MM-DD'))
            }
            
            set(dataForm, 'CustomerId', toString(get(dataForm, 'CustomerId')))
            const result = await put(`http://localhost:8080/api/contract/${id}`, dataForm);
            if (eq(200, get(result, 'status'))) {
                notification.success({ message: 'เพิ่มข้อมูลสำเร็จ' })
                window.location.href = `/contract/${id}`;
            }
        } catch (error) {
            const description = get(error, 'response.data.error')
            notification.error({ message: 'เกิดข้อผิดพลาด', description })
        }
    };

    useEffect(async () => {
        if (isNil(id)) return 
        const result = get(await axios(`http://localhost:8080/api/contract/${id}`), 'data')
        set(result, 'StartDate', moment(get(result, 'StartDate')))
        set(result, 'EndDate', moment(get(result, 'EndDate')))
        setData(result);

        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, [id]);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }

    return <>
        <PageHeader
            onBack={() => { window.location.href = `/contract` }}
            title="แก้ไขสัญญา"
        >
            <Form layout="horizontal" onFinish={onFinish} initialValues={data}>
                <FormItem
                    name="CustomerId"
                    label="ลูกค้า"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <Select defaultValue={data?.CustomerId}>
                        {map(customers, ({ id, CustomerName }) => <Select.Option key={id} value={toString(id)}>{CustomerName}</Select.Option>)}
                    </Select>
                </FormItem>
                <FormItem
                    name="StartDate"
                    label="วันเริ่มสัญญา"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <DatePicker format={dateFormat} />
                </FormItem>
                <FormItem
                    name="EndDate"
                    label="วันสิ้นสุดสัญญา"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 8 }}
                >
                    <DatePicker format={dateFormat} />
                </FormItem>

                <FormItem
                    style={{ marginTop: 48 }}
                    wrapperCol={{ span: 8, offset: 8 }}
                >
                    <Button size="large" type="primary" htmlType="submit" block>
                        แก้ไข
                    </Button>
                </FormItem>
            </Form>
        </PageHeader>
    </>
}

export default withLayout(ConteactEdit, { title: 'ConteactEdit' })