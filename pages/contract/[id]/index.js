import { useState, useEffect } from 'react';
import { Descriptions, Tag, Space, PageHeader, Button, Layout, Spin } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from 'axios';
import { get, isNil, find, toInteger, includes } from 'lodash'
import withLayout from '../../../hoc/withLayout';
import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../../../atom/state';
import moment from 'moment';
import 'moment/locale/th' 
moment.locale('th')

const ContractView = () => {
    const { id } = useRouter().query
    const [data, setData] = useState()

    const [customers, setCustomers] = useState([])
    const [user, setUser] = useRecoilState(authState);

    useEffect(async () => {
        if (isNil(id)) return 
        const result = await axios(`http://localhost:8080/api/contract/${id}`);
        setData(get(result, 'data'));

        setCustomers(get(await axios('http://localhost:8080/api/customer'), 'data', []));
    }, [id]);

    if (isNil(data)) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <Spin tip="กำลังร้องขอข้อมูล..." />
            </div>
        )
    }


    return (
        <Layout.Content style={{ padding: '0 50px', background: 'white' }}>
            <PageHeader
                onBack={() => { window.location.href = `/contract` }}
                title="รายละเอียดสัญญา"
                extra={[
                    includes([0, 2], user?.Case) && (
                        <Link href={`/contract/${id}/edit`}>
                            <a>แก้ไข</a>
                        </Link>
                    )
                ]}
            >
                <Descriptions bordered size="small">
                    <Descriptions.Item label="รหัสปัญหา">
                        {data?.id}
                    </Descriptions.Item>
                    <Descriptions.Item label="ลูกค้า" span="2">
                        {find(customers, ['id', toInteger(data?.CustomerId)])?.CustomerName || data?.CustomerId}
                    </Descriptions.Item>
                    <Descriptions.Item label="วันเริ่มสัญญา">
                        {!!data?.StartDate && moment(data?.StartDate).format('ll')}
                    </Descriptions.Item>
                    <Descriptions.Item label="วันสิ้นสุดสัญญา">
                        {!!data?.EndDate && moment(data?.EndDate).format('ll')}
                    </Descriptions.Item>
                </Descriptions>
            </PageHeader>
        </Layout.Content>
    )
    
    
}
export default withLayout(ContractView, { title: 'ContractView' })