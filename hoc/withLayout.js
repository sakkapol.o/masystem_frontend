import { Button, Layout, Col, Row, Tag } from 'antd'
import { useState, useEffect } from 'react'
import jwt_decode from "jwt-decode"
import Cookies from 'js-cookie'
import Link from 'next/link'

import { atom, selector, useRecoilState } from 'recoil';
import { authState } from '../atom/state'

const { Header, Footer, Sider, Content } = Layout;

export const ROLE = [
    'แอดมิน',
    'ฝ่ายบริหาร',
    'ฝ่ายประสานงาน',
    'ฝ่ายดูแลระบบ',
    'ลูกค้า'
]


const withLayout = (Component, { title = 'Hi!' }) => {
    
    return (props) => {
        
        const [user, setUser] = useRecoilState(authState);

        const signOut = () => {
            Cookies.remove('token');
            window.location.href = `/auth`;
        }

        useEffect(() => {
            try {
                setUser(jwt_decode(Cookies.get('token')))
            } catch (error) {
                window.location.href = `/auth`;
            }
        }, [])

        

        return (
            <Layout>
                <Header style={{ color: 'white', textAlign: 'right' }}>
                    
                    <Link href="/" style={{ cursor: 'pointer' }}>
                        <a>
                            <Tag style={{ float: 'left', margin: '20px' }}>MA SYSTEM</Tag>
                        </a>
                    </Link>
                    <Tag color="#2db7f5" className="ml-1">{user?.userId} </Tag>
                    <Tag color="cyan" className="ml-1">{ROLE[user?.Case]} </Tag>
                    <Button style={{ color: 'white', textAlign: 'right' }} type="primary" danger onClick={signOut} className="ml-2" size="small">
                        ออกจากระบบ
                    </Button>
                </Header>
                <Content>
                    <Row>
                        <Col span={16} offset={4}>
                            <Component {...props} />
                        </Col>
                    </Row>
                </Content>
            </Layout>
        )
    }
}

export default withLayout